﻿using UnityEngine;
using System.Collections;

public class CameraScroll: MonoBehaviour {
	
	public int Boundary  = 5; // distance from edge scrolling starts
	public int minBoundary=0;
	public float speed = 15;
	private int theScreenWidth;
	private int theScreenHeight;
	public Texture2D arrow;
	void Start() 
	{
		theScreenWidth = Screen.width;
		theScreenHeight = Screen.height;
	}

	void Update() 
	{
		Debug.Log (Input.mousePosition.x);
		bool isMouseInScreen = (Input.mousePosition.x < theScreenWidth && Input.mousePosition.x > 0 &&
		                       Input.mousePosition.y < theScreenHeight && Input.mousePosition.y > 0);

			//the camera size is 
			const int orthographicSizeMin = 1;
			const int orthographicSizeMax = 6;

			if (isMouseInScreen && Input.GetAxis("Mouse ScrollWheel") > 0) // forward
			{
				Camera.main.orthographicSize++;
			}
			if (isMouseInScreen && Input.GetAxis("Mouse ScrollWheel") < 0) // back
			{
				Camera.main.orthographicSize--;
			}
			Camera.main.orthographicSize = Mathf.Clamp(Camera.main.orthographicSize, orthographicSizeMin, orthographicSizeMax );
		speed = (int) Camera.main.orthographicSize * 1.5f;

		//make it so that the camera can only move so that the corner matches the corner, but this depends
		//on orthographic size

		//the orthographic size is the y radius; the size * screenwidth / screenheight is the x radius
		Vector3 oldPos = transform.position;

		if (Input.mousePosition.x > theScreenWidth - Boundary && Input.mousePosition.x<theScreenWidth-minBoundary)
		{
			transform.position += new Vector3( speed * Time.deltaTime,0,0); // move on +X axis
			Cursor.SetCursor(arrow, Vector2.zero, CursorMode.Auto);
		}
		if (Input.mousePosition.x < 0 + Boundary && Input.mousePosition.x>minBoundary)
		{
			transform.position -= new Vector3(speed * Time.deltaTime,0,0); // move on -X axis
		}
		if (Input.mousePosition.y > theScreenHeight - Boundary && Input.mousePosition.y<theScreenHeight-minBoundary)
		{
			transform.position += new Vector3(0,speed * Time.deltaTime,0); // move on +Z axis
		}
		if (Input.mousePosition.y < 0 + Boundary && Input.mousePosition.y>minBoundary)
		{
			transform.position -= new Vector3(0,speed * Time.deltaTime,0); // move on -Z axis
		}

		//the min x position 0; the max is 0+size.x

		//the min location is so that the square corner 

		Vector3 cameraLeft = Camera.main.ViewportToWorldPoint (new Vector3(0, 0, 0));
		Vector3 cameraRight = Camera.main.ViewportToWorldPoint (new Vector3(1, 1, 0));

		//If the camera is at 1,0 with a left of -1 and right of 3, then the minimum position is 0
		//min position = min bound + (cameraPos-cameraLeft)
		float minX =  (transform.position.x - cameraLeft.x);
		float maxX = GameManager.instance.maxBounds.x - (cameraRight.x - transform.position.x)-1;
		float minY =  (transform.position.y - cameraLeft.y);
		float maxY = GameManager.instance.maxBounds.y - (cameraRight.y - transform.position.y)-1;



		transform.position = new Vector3 (clampVal(transform.position.x, minX,maxX) , clampVal(transform.position.y,minY,maxY), transform.position.z);
	//	}
	//	if (cameraLeft.y < -1 || cameraRight.y > GameManager.instance.maxBounds.y + 1) {
	//		transform.position = new Vector3 (transform.position.x, oldPos.y, transform.position.z);
	//	}

		//Vector3 cameraTop = Camera.main.ViewportToWorldPoint (0, 1, 0);




		//transform.position = clampVector (transform.position, GameManager.instance.minBounds, GameManager.instance.minBounds);

	}  
	private float clampVal(float val, float min, float max){
		return Mathf.Max(Mathf.Min(val, max), min);
	}

	private Vector3 clampVector (Vector3 vec, Vector2 min, Vector2 max){
		return new Vector3 (Mathf.Max (Mathf.Min (vec.x, max.x), min.x),
			Mathf.Max (Mathf.Min (vec.y, max.y), min.y), vec.z); 
	}


	void OnGUI() 
	{
	//	GUI.Box( Rect( (Screen.width / 2) - 140, 5, 280, 25 ), "Mouse Position = " + Input.mousePosition );
	//	GUI.Box( Rect( (Screen.width / 2) - 70, Screen.height - 30, 140, 25 ), "Mouse X = " + Input.mousePosition.x );
	//	GUI.Box( Rect( 5, (Screen.height / 2) - 12, 140, 25 ), "Mouse Y = " + Input.mousePosition.y );
	}

}
