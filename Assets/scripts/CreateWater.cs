﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


public class CreateWater : MonoBehaviour {
	public GameObject water; 
	// Use this for initialization
	void Awake () {
	//	float height = Camera.main.orthographicSize * 2.0f;
	//	float width = height * Camera.main.aspect;
		float width = 20;
		float x_size = water.transform.GetChild(0).GetComponent<SpriteRenderer> ().bounds.extents.x*2;
		float x_1 = 0;
		GameObject prevOb = null;
		while (x_1 < width){
			x_1 += x_size;
			GameObject g = Instantiate(water, new Vector3(transform.position.x + x_1, this.transform.position.y, 0), Quaternion.identity) as GameObject;
			GameObject ws = g.GetComponentInChildren<WaveScript> ().gameObject;
			if (prevOb != null) {
				ws.GetComponent<WaveScript> ().setNeighbor (1, prevOb);
				prevOb.GetComponent<WaveScript> ().setNeighbor (2, ws);
			}
			prevOb = ws;
				}
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
