﻿Shader "Custom/TextureCoordinates/mask_fading" {
	Properties {
		_Color ("Main Color", Color) = (1,1,1,1)
		[PerRendererData] _MainTex ("Sprite Texture", 2D) = "white" {}
		_MainTex2 ("Texture2", 2D) = "white" {}
		_TexScale("TexScale", Float) = 1

	}

    SubShader
    {
        Tags
        {
            "Queue"="Transparent"
            "IgnoreProjector"="True"
            "RenderType"="Transparent"
            "PreviewType"="Plane"
            "CanUseSpriteAtlas"="True"
        }

        Cull Off
        Lighting Off
        ZWrite Off
        Blend One OneMinusSrcAlpha

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #pragma multi_compile _ PIXELSNAP_ON
            #pragma shader_feature ETC1_EXTERNAL_ALPHA
            #include "UnityCG.cginc"

            struct appdata_t
            {
                float4 vertex   : POSITION;
                float4 color    : COLOR;
                float2 texcoord : TEXCOORD0;
            };

            struct v2f
            {
                float4 vertex   : SV_POSITION;
                fixed4 color    : COLOR;
                float2 texcoord  : TEXCOORD0;
                float2 maskcoord : TEXCOORD4;
                float3 worldPos: TEXCOORD1;
                float3 localPos: TEXCOORD2;
            };

            fixed4 _Color;
            float _Outline;
            fixed4 _OutlineColor;
                       sampler2D _MainTex;
            sampler2D _MainTex2;
            sampler2D _AlphaTex;
            float4 _MainTex_TexelSize;
             float4 _MainTex2_TexelSize;
             float _TexScale;
             float4 _MainTex_ST;
             float4 _MainTex2_ST;


            v2f vert(appdata_t IN)
            {
                v2f OUT;
                OUT.vertex = mul(UNITY_MATRIX_MVP, IN.vertex);
                float4 scaleVertex = float4(IN.vertex.xyz, 0); //By setting the last value to 0 it ignores the flipping ( loses relative position if sprite is flipped :( )
float4 wP = mul(_Object2World, scaleVertex); //Get the object to world vertex and store it
OUT.worldPos = wP.xyz; //For use in fragment shader
float4 lP = mul(_World2Object, scaleVertex); //Get the world to object vertex and store it
OUT.localPos = lP.xyz; //For use in fragment shader
 
 OUT.texcoord = TRANSFORM_TEX(IN.texcoord, _MainTex);
  OUT.maskcoord = TRANSFORM_TEX(IN.texcoord, _MainTex);

OUT.maskcoord = TRANSFORM_TEX(float2(IN.vertex.x / _ScreenParams.x, IN.vertex.y/_ScreenParams.y), _MainTex2);
 
            //    OUT.texcoord = IN.texcoord;
                OUT.color = IN.color * _Color;
                #ifdef PIXELSNAP_ON
                OUT.vertex = UnityPixelSnap (OUT.vertex);
                #endif

                return OUT;
            }
          
              fixed4 SampleSpriteTexture (float2 uv)
            {
                fixed4 color = tex2D (_MainTex, uv);

                #if ETC1_EXTERNAL_ALPHA
                // get the color from an external texture (usecase: Alpha support for ETC1 on android)
                color.a = tex2D (_AlphaTex, uv).r;
                #endif //ETC1_EXTERNAL_ALPHA

                return color;
            }

		 	 float mod(float x, float y)
			{
			  return x - floor(x * (1.0 / y)) * y;
			}

            fixed4 frag(v2f IN) : SV_Target
            {
                fixed2 relativeWorld = fixed2(IN.worldPos.x + IN.localPos.x, IN.worldPos.y + IN.localPos.y);
//                    fixed2 relativePos = fixed2((relativeWorld.x + _Width), (relativeWorld.y + _Height));

          	  fixed4 c = SampleSpriteTexture (IN.texcoord) * IN.color;
            	if(c.a < .0 || c.r+c.g+c.b < 0.1){
            		return float4(c.rgb,c.a);
            		}
            		else{
            	//float3 color = tex2D(_MainTex2, float2(IN.vertex.x/_ScreenParams.x, IN.vertex.y/_ScreenParams.y)).rgb;
            	//float3 color = tex2D(_MainTex2, _Tex_Scale*float2(IN.localPos.x + _DisplaceX, IN.localPos.y + _DisplaceY)).rgb;
            	//float3 color = tex2D(_MainTex2, float2(IN.maskcoord.x / _MainTex2_TexelSize.z, IN.maskcoord.y / _MainTex2_TexelSize.w)).rgb;

            //	float3 color = tex2D(_MainTex2, float2(IN.vertex.x / _ScreenParams.x, IN.vertex.y / _ScreenParams.y)).rgb;
            	//float3 color = tex2D(_MainTex2, IN.maskcoord).rgb;
            	float xCoor = ((IN.vertex.x*1/_TexScale) % _MainTex2_TexelSize.z)/_MainTex2_TexelSize.z;
            	float yCoor = ((IN.vertex.y*1/_TexScale) % _MainTex2_TexelSize.w)/_MainTex2_TexelSize.w;
            	float3 color = tex2D(_MainTex2, float2(xCoor,yCoor)).rgb;
            	float alpha = IN.texcoord / 40;
            	//color.rgb = IN.texcoord.y*10;
            	//color.rgb = IN.texcoord.x/2;
            	//color.rgb = _MainTex_TexelSize.x * IN.vertex.x;
            	//color.rgb = (_MainTex_TexelSize.z/400) ;
            	//color.rgb = IN.localPos.x + 10;//_MainTex_TexelSize.z;
            	//color.rgb = 
            	return float4(color*.1,(_ScreenParams.y-IN.vertex.y)/_ScreenParams.y*2 - .7);
            	//top is 0, bottom is 2, middle is 1 - 
            	//(100-100) = 0, top
            	//(100 - 0) = 1, bottom


            	// top is whatever, middle is .2, bottom is 1
            	}
            	}



            ENDCG
        }
        }
        }
 

