﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class load_main : MonoBehaviour {
	public string sceneToLoad;
	void OnMouseDown() {
		SceneManager.LoadScene (sceneToLoad);
		GameManager.instance.Reset ();
	}
}