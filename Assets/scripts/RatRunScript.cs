﻿using UnityEngine;
using System.Collections;

public class RatRunScript : MonoBehaviour {
	public float turnAroundTime;
	public float currentTime=0;
	public float velocity;
	public bool Survived = false;

	// Use this for initialization
	void Start () {
	
	}

	void turnAroundRat(){
		GetComponent<SpriteRenderer> ().flipX = !GetComponent<SpriteRenderer> ().flipX;
		velocity = -velocity;
	}

	void OnTriggerEnter2D(Collider2D otherCollider){
		if(otherCollider.gameObject.tag == "trampoline") {
			Debug.Log ("trampoline");

			GetComponent<Rigidbody2D> ().velocity += new Vector2 (0, -2*GetComponent<Rigidbody2D> ().velocity.y);  //AddForce(new Vector2(0.0f,700.0f));
		}
		else if (otherCollider.gameObject.tag == "platform_end") {
			if(!GetComponent<Animator>().GetBool("inAir")){
			GetComponent<Animator>().SetBool("inAir",true);
			GetComponent<Rigidbody2D> ().AddForce (new Vector2 (0, 300));
			}
		}
		else  {
			GetComponent<Animator>().SetBool("inAir",false);
		}
		if (otherCollider.gameObject.tag == "shore") {
			SpriteRenderer[] all = GetComponentsInChildren<SpriteRenderer> ();
			for (int i = 0; i < all.Length; i++) {
				all [i].sortingOrder = 100;
			}
			if (!Survived) {
				Survived = true;
				GameManager.instance.ratsSaved += 1;
			}
			GetComponent<SpriteRenderer> ().sortingOrder = 100;
		} else if (otherCollider.gameObject.tag == "rat_graveyard") {
			GameManager.instance.ratsDead += 1;
			Destroy (gameObject);
		}

			}
		

	// Update is called once per frame
	void Update () {
		GetComponent<Rigidbody2D> ().velocity = new Vector2(velocity,GetComponent<Rigidbody2D> ().velocity.y);
		currentTime = currentTime + Time.deltaTime;
		//transform.position += new Vector3 ( velocity, 0, 0);

			if(currentTime>turnAroundTime){
				currentTime=0;
				//turnAroundRat();
			}
	}
}
