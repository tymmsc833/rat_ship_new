﻿using UnityEngine;
using System.Collections;

public class LightPulsingScript : MonoBehaviour {
	private float currentScale; 
	public float GROW_TOTAL_SECS = 2.0f;
	public float pulse_size = 0.0f; 
	public Vector3 startingScale;


	// Use this for initialization
	void Start (){
		startingScale = transform.localScale;
	}
	
	// Update is called once per frame
	void Update () {

		//Debug.Log ("Time: " + Time.time);

		//float rateTime = Time.time % GROW_TOTAL_SECS; //value between 0 and grow_total_seconds
		//float pulse = Mathf.Abs(rateTime / GROW_TOTAL_SECS * 2.0f - 1.0f) ; //now it goes from 0 to 1 and back to 0 again 

		//pulse = pulse * 2.0f - 1.0f; // value between -1 and 1
		//pulse = pulse * pulse_size;  //value between -pulse_size and pulse_size

		//Debug.Log(" PULSE: " + pulse); 

		Vector3 scale = startingScale + new Vector3 (Mathf.PerlinNoise(Time.time *2,0)+1, Mathf.PerlinNoise(Time.time *2,0)+1, 1.0f);
		transform.localScale = scale;
	}
}
