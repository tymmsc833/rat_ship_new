﻿/*using System.Collections.Generic;
using System.Linq;
using UnityEngine;

/// <summary>
/// Parallax scrolling script that should be assigned to a layer
/// </summary>
public class ScrollingScript : MonoBehaviour
{
	/// <summary>
	/// Scrolling speed
	/// </summary>
	private Vector2 speed;

	public GameObject background;

	public Dictionary<string, GameObject> roadDict = new Dictionary<string,GameObject>();
	public GameObject cycloid;
	public GameObject flat; 
	public GameObject sawtooth;
	public GameObject square;
	public GameObject hexagon; 
	public GameObject centerEllipse;
	public GameObject offCenterEllipse;
	public GameObject invisible;

	public int numParts;

//	private RoadScript roadScript; 

	/// <summary>
	/// Moving direction
	/// </summary>
	public Vector2 direction = new Vector2(-1, 0);

	/// <summary>
	/// Movement should be applied to camera
	/// </summary>
	public bool isLinkedToCamera = false;

	/// <summary>
	/// 1 - Background is infinite
	/// </summary>
	public bool isLooping = false;

	/// <summary>
	/// 2 - List of children with a renderer.
	/// </summary>
	private List<SpriteRenderer> backgroundPart;

	// 3 - Get all the children

	void initDictionary(){
		roadDict.Add ("cycloid", cycloid);
		roadDict.Add ("flat", flat);
		roadDict.Add ("sawtooth", sawtooth);
		roadDict.Add ("square", square);
		roadDict.Add ("hexagon", hexagon);
		roadDict.Add ("centerEllipse", centerEllipse);
		roadDict.Add ("offCenterEllipse", offCenterEllipse);
		roadDict.Add ("invisible", invisible);

	}
	void Awake()
	{
		initDictionary ();


	}

	void Start(){
		roadScript = GetComponent<RoadScript> ();
		initRoad (GameManager.instance.currentRoad);

	}

	public void initRoad(string roadtype){
		background = roadDict [roadtype];

		//duplicate this object
		for (int i = 0; i < numParts; i++) {
			GameObject bg = Instantiate (background) as GameObject;
			float xSize = getPartSize ();
			bg.transform.Translate (new Vector3 (i*xSize-2*xSize, 0, 0));
			bg.transform.parent = this.transform;
		}

		// For infinite background only
		if (isLooping)
		{
			// Get all the children of the layer with a renderer
			backgroundPart = new List<SpriteRenderer>();


			for (int i = 0; i < transform.childCount; i++)
			{


				Transform child = transform.GetChild(i);
				SpriteRenderer r = child.GetComponent<SpriteRenderer>();

				// Add only the visible children
				if (r != null)
				{
					backgroundPart.Add(r);
				}
			}

			// Sort by position.
			// Note: Get the children from left to right.
			// We would need to add a few conditions to handle
			// all the possible scrolling directions.
			backgroundPart = backgroundPart.OrderBy(
				t => t.transform.position.x
			).ToList();
		}
		float x_Size = getPartSize ();
		speed = new Vector2 (x_Size, 0);
	}

	void Update()
	{

		// Movement
		Vector3 movement = new Vector3(
			GameManager.instance.systemSpeed * speed.x * direction.x,
			GameManager.instance.systemSpeed * speed.y * direction.y,
			0);

		movement *= Time.deltaTime;
		transform.Translate(movement);

		// Move the camera
		if (isLinkedToCamera)
		{
			Camera.main.transform.Translate(movement);
		}

		// 4 - Loop
		if (isLooping)
		{
			// Get the first object.
			// The list is ordered from left (x position) to right.
			SpriteRenderer firstChild = backgroundPart.FirstOrDefault();

			if (firstChild != null)
			{
				// Check if the child is already (partly) before the camera.
				// We test the position first because the IsVisibleFrom
				// method is a bit heavier to execute.
				if (firstChild.transform.position.x < Camera.main.transform.position.x)
				{
					// If the child is already on the left of the camera,
					// we test if it's completely outside and needs to be
					// recycled.
					if (firstChild.isVisible == false)
					{
						// Get the last child position.
						SpriteRenderer lastChild = backgroundPart.LastOrDefault();

						Vector3 lastPosition = lastChild.transform.position;
						float obSize = getPartSize ();

						// Set the position of the recyled one to be AFTER
						// the last child.
						// Note: Only work for horizontal scrolling currently.
						backgroundPart.Remove(firstChild);

						Destroy(firstChild.gameObject);
						GameObject newChild = Instantiate (roadDict[roadScript.GetNextPartPlatform()]) as GameObject;
						newChild.transform.parent = this.transform;
						newChild.transform.position = new Vector3(lastPosition.x + obSize, newChild.transform.position.y, firstChild.transform.position.z);

						// Set the recycled child to the last position
						// of the backgroundPart list.
						backgroundPart.Add(newChild.GetComponent<SpriteRenderer>());
					}
				}
			}
		}
	}
	public void setBackground(string s){
		background = roadDict [s];
		float x_Size = getPartSize ();
		speed = new Vector2 (x_Size, 0);

		foreach (SpriteRenderer part in backgroundPart) {
			part.sprite = background.GetComponent<SpriteRenderer> ().sprite;
			part.transform.position = new Vector3 (part.transform.position.x, background.transform.position.y, part.transform.position.z);
		}
	}
	public float getPartSize(){
		
	//	Debug.Log( "PART SIZE: "+background.name + " "+ background.GetComponent<SpriteRenderer>().bounds.size [0]);
		return 1.76f;
	}

	//the shape spins at x degrees per second.

	//the background moves at 1 time per second. 
}
*/
