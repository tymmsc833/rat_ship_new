﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour {
	public static GameManager instance = null;				//Static instance of GameManager which allows it to be accessed by any other script.

	public Vector2 size;
	public Vector2 minBounds;
	public Vector2 maxBounds;
	public float systemSpeed;
	public int ratsSaved = 0;
	public int ratsDead = 0; 
	public int ratsSpawned = 0;
	public int maxRatsSpawned = 6;
	public bool gameOver = false;

	void Awake()
	{
		//Cursor.visible = false;
		//Cursor.SetCursor (cursor, new Vector2 (0, 0), CursorMode.ForceSoftware);
		//Curso
		minBounds = new Vector2 (0, 0);
		maxBounds = new Vector2 (size.x, size.y);
		//Check if instance already exists
		if (instance == null)
		{

			//if not, set instance to this
			instance = this;
		//	Instantiate (GardenManager);

	}
		//If instance already exists and it's not this:
		else if (instance != this)

			//Then destroy this. This enforces our singleton pattern, meaning there can only ever be one instance of a GameManager.
			Destroy(gameObject);	

		//Sets this to not be destroyed when reloading scene
		//DontDestroyOnLoad(gameObject);


	}
	public void Reset(){
		ratsSaved = 0;
		ratsDead = 0; 
		ratsSpawned = 0;
		maxRatsSpawned = 6;
		gameOver = false;
	}
		

	// Update is called once per frame
	void Update () {
		if (ratsSpawned == maxRatsSpawned && ratsDead+ratsSaved>=ratsSpawned){
			if (!gameOver) {
				gameOver = true;
			}
		}
			
	}
}
