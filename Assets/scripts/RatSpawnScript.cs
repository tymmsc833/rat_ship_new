﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class RatSpawnScript : MonoBehaviour {

	public GameObject ratPrefab;
	public float spawnTime=1.0f;

	// Use this for initialization
	void Start () {

		InvokeRepeating ("Spawn", spawnTime, spawnTime);

	}



	void Spawn ()
	{
		if (!(GameManager.instance.maxRatsSpawned==GameManager.instance.ratsSpawned)) {
			GameObject rat = Instantiate (ratPrefab, transform.position + new Vector3 (Random.Range (-.1f, .1f), Random.Range (-.2f, .2f), 0), Quaternion.identity) as GameObject;
			GameManager.instance.ratsSpawned += 1;
		}
	}




	// Update is called once per frame
	void Update () {
		//for each species, calculate how likely it is to spawn a new butterfly based on commonness, plants, and how many have hatched


	}
}
