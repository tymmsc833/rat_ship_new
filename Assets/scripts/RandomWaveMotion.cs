﻿using UnityEngine;
using System.Collections;

public class RandomWaveMotion : MonoBehaviour {
	public float maxY; 
	public float minY; 
	public Rigidbody2D rb;
	public float origY;
	// Use this for initialization
	void Start () {
		rb = GetComponent<Rigidbody2D> ();
		origY = transform.position.y;
	}
	
	// Update is called once per frame

	void Update () {
		rb.position = new Vector2(rb.position.x,origY+Mathf.PerlinNoise (Time.time, 0));

		}
}
