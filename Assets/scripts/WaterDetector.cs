﻿using UnityEngine;
using System.Collections;

public class WaterDetector : MonoBehaviour {
	public float points = 15;
	public float power = .02f; 
    void OnTriggerEnter2D(Collider2D Hit)
    {
		if (Hit.GetComponent<Rigidbody2D>() != null)
        {
			Debug.Log (Hit.gameObject.name);
			transform.parent.GetComponent<Water>().Splash(transform.position.x, Hit.GetComponent<Rigidbody2D>().velocity.y*Hit.GetComponent<Rigidbody2D>().mass / 40f);
        }
    }

	void Update(){
		Debug.Log ("mouse over");
		if (Input.GetMouseButtonDown (0)) {
			Debug.Log ("add ");
			Vector3 ms = Camera.main.ScreenToWorldPoint (Input.mousePosition + new Vector3 (0, 0, 0));
			for (int i = 0; i < points; i++)
			{
				transform.parent.GetComponent<Water> ().MakeWave (ms.x + (i-points/2)/points, power/points);
			}


		}
	}/*
    /*void OnTriggerStay2D(Collider2D Hit)
    {
        //print(Hit.name);
        if (Hit.rigidbody2D != null)
        {
            int points = Mathf.RoundToInt(Hit.transform.localScale.x * 15f);
            for (int i = 0; i < points; i++)
            {
                transform.parent.GetComponent<Water>().Splish(Hit.transform.position.x - Hit.transform.localScale.x + i * 2 * Hit.transform.localScale.x / points, Hit.rigidbody2D.mass * Hit.rigidbody2D.velocity.x / 10f / points * 2f);
            }
        }
    }*/

}
