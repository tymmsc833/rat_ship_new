﻿using UnityEngine;
using System.Collections;

public class ObjectFloatSink : MonoBehaviour
{
	public float sinkingRate = 1;
	public float floatingRate=10.5f;
	public float done = .1f;
	public bool beginSinking = false;
	public bool isSunk=false;
	public float sinkingLevel; 
	public BoxCollider2D mycollider;
	private Vector2 offset_orig;
	private Vector2 size_orig;


	// Use this for initialization
	void Start ()
	{
	//	mycollider = GetComponent<BoxCollider2D> ();
		offset_orig = mycollider.offset;
		size_orig = mycollider.size;
	
	}

	void OnTriggerEnter2D(Collider2D otherCollider){
		if(otherCollider.gameObject.tag == "bubble") {
			Debug.Log ("bubble");

			GetComponent<Rigidbody2D> ().AddForce (new Vector2(0,4));
			if (!isSunk) {
				mycollider.offset -= new Vector2 (0, .5f * floatingRate);
				mycollider.size += new Vector2 (0, floatingRate);
				mycollider.offset = new Vector2 (mycollider.offset.x, Mathf.Max (mycollider.offset.y, offset_orig.y));
				mycollider.size = new Vector2 (mycollider.size.x, Mathf.Min (mycollider.size.y, size_orig.y));

			}
		}
		if (otherCollider.gameObject.tag == "rat") {
			beginSinking = true;
		}
	}

	//if it's in the air the gravity is normal, also the collider resets
	//if it's in the water gravity is lowr



	// Update is called once per frame
	void Update ()
	{
		if (beginSinking) {
			mycollider.size = new Vector2 (mycollider.size.x, Mathf.Max (mycollider.size.y, 0.000001f));

			//change the bounding box size
			mycollider.offset += new Vector2 (0, .5f * sinkingRate);
			mycollider.size -= new Vector2 (0, sinkingRate);
			if (mycollider.size.y < done) {
				isSunk = true;
			}
		}
		//shrink the bound 

		//make it sink at the sinking rate (rate is higher if rats are touching it - multiplier?)
		// if 
	}
}

