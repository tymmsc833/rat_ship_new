﻿using UnityEngine;
using System.Collections;


public class Mouse : MonoBehaviour
{
	public GameObject cursorSprite; 
	private Vector2 sensitivity;

	// Use this for initialization
	void Start ()
	{
		sensitivity = new Vector2(0.9f, 0.9f);

	}
	
	// Update is called once per frame
	void Update ()
	{
			Vector2 mouseMovement = new Vector2(Input.GetAxisRaw("Mouse X") * sensitivity.x,Input.GetAxisRaw("Mouse Y") * sensitivity.y);

		cursorSprite.transform.position += new Vector3(mouseMovement.x, mouseMovement.y, 0);
	}
}

