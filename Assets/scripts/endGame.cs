﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class endGame : MonoBehaviour
{

	// Use this for initialization
	void Start ()
	{
	
	}
	
	// Update is called once per frame
	void Update ()
	{
		if (GameManager.instance.gameOver || Input.GetKey(KeyCode.R)) {
			SceneManager.LoadScene ("win screen");
		}
	}
}

