﻿using UnityEngine;
using System.Collections;

public class WaveScript : MonoBehaviour {
	public GameObject neighbor1;
	public GameObject neighbor2;
	public float damping = 0;
	// Use this for initialization
	void Start () {
	
	}
	public void setNeighbor (int num, GameObject go){
		if (num == 1) {
			neighbor1 = go;
		} else {
			neighbor2 = go;
		}
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	public void CreateWave(int dir, float force){
		//if (GetComponent<Rigidbody2D> ().velocity.y == 0) {
			GetComponent<Rigidbody2D> ().AddForce (new Vector2 (0, force));
			StartCoroutine(makeWaveNeighbors (.05f, dir, Mathf.Max(0,force-damping)));

	//	}
	


	}
	private IEnumerator makeWaveNeighbors(float lag, int dir, float force){
		yield return new WaitForSeconds (lag);
		if (neighbor1 != null && dir <= 0) {
			neighbor1.GetComponent<WaveScript> ().CreateWave (-1, force);
		}
		if (neighbor2 != null && dir>=0) {
			neighbor2.GetComponent<WaveScript> ().CreateWave (1,force);
		}

	}
		
}
